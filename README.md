# Buenos Aires House Price Predictor

This is the final report to graduate from the [Data Science](https://www.acamica.com/data-science)
career at Acámica.

Using all the tools acquired in the career, an analysis should be performed in order to predict
house prices in Buenos Aires.

The steps to fulfill the minimum requirements were:

* State at least one new objective to continue the analysis of the problem.
* Justify any actions and transformations applied to the data.
* Each provided answer should have its corresponding explicit question.
